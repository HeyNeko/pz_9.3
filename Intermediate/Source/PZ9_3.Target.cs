using UnrealBuildTool;

public class PZ9_3Target : TargetRules
{
	public PZ9_3Target(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("PZ9_3");
	}
}
